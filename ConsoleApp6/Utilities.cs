﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using Microsoft.Win32;
using TCX.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Ini;


namespace OMSamples
{
    public static class Utilities
    {


        public static string GetVal(string section,string keyName,string Filenaname)
        {

            var dir = Path.GetDirectoryName(Filenaname);

            var builder = new ConfigurationBuilder()
                .SetBasePath(dir)
                .AddIniFile("3CXPhoneSystem.ini", optional: false);
            var config = builder.Build();
            var res = config[section+ ":" + keyName];
            return res;
        }

        static public string GetKeyValue(string Section, string KeyName, string FileName)
        {
            //Reading The KeyValue Method
            try
            {
                StringBuilder JStr = new StringBuilder(255);
                return GetVal(Section, KeyName, FileName);
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        public static string GetParameterValue(string name, string DefaultValue, ref bool exists)
        {
            try
            {
                exists = true;
                return TCX.Configuration.PhoneSystem.Root.GetParameterByName(name).Value;
            }
            catch
            {
                exists = false;
                return DefaultValue;
            }
        }
    }
}
