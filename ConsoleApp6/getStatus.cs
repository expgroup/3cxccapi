﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TCX.Configuration;
using Quobject.SocketIoClientDotNet.Client;

namespace OMSamples
{
    class getStatus
    {
        public void Run(params string[] args)
        {
            {
                MyListener aa = new MyListener(null);
                PhoneSystem.Root.Updated += new NotificationEventHandler(aa.ps_Updated);
                PhoneSystem.Root.Inserted += new NotificationEventHandler(aa.ps_Inserted);
                PhoneSystem.Root.Deleted += new NotificationEventHandler(aa.ps_Deleted);
            }

            //Connect to Socket.IO?
            var lesocket = IO.Socket("http://10.254.1.7:3035");
            lesocket.On(Socket.EVENT_CONNECT, () =>
            {
                lesocket.Emit("3CXConn");
            });

            foreach (Extension a in PhoneSystem.Root.GetTenants()[0].GetExtensions())
            {
                if (a.IsRegistered)
                {
                    DN thisdn = PhoneSystem.Root.GetDNByNumber(a.Number);
                    //System.Console.WriteLine("Groups = " + a.Groups[2]);
                    foreach (var gr in a.Groups)
                    {
                        var pattern = "__DEFAULT__";
                        var input = gr.ToString();
                        if (pattern.Equals(input))
                        //if (gr.ToString() == defGrName)
                        {
                            System.Console.WriteLine("Belongs to Default Group");
                            //SendData("Belongs to " + defGrName +"\n");
                        }
                        else
                        {
                            System.Console.WriteLine("Belongs to: " + gr.ToString());
                            //SendData("Belongs to: " + gr.ToString() + "\n");
                            var grps = gr.ToString();
                            lesocket.Emit("groups", grps);


                        }

                    }


                    foreach (var pr in a.GetActiveConnections())
                    {
                        System.Console.WriteLine("active - " + pr.LastChangeStatus);
                        //SendData("active - " + pr.Status + "\n");
                        var liveCall = "{\"extension\":\"" + a.Number + "\", \"status\":\"" + pr.Status + "\"}";
                        lesocket.Emit("Call", liveCall);
                        //Thread.Sleep(1000);
                        System.Console.WriteLine("active - " + pr.Status);
                    }

                    System.Console.WriteLine("Extension " + a.Number + " " + a.FirstName + " " + a.LastName + " => " + a.CurrentProfile.Name);
                    //var extMsg = "{\"extension\":\"" + a.Number + "\", \"firstname\":\"" + a.FirstName + "\", \"lastname\":\"" + a.LastName + "\", \"status\":\"" + a.CurrentProfile.Name + "\", \"qstatus\":\"" + a.QueueStatus + "\"}";
                    //lesocket.Emit("extUpd", extMsg);
                    //SendData("{\"extension\":\""+a.Number+"\", \"firstname\":\""+a.FirstName+"\", \"lastname\":\""+a.LastName+"\", \"status\":\""+a.CurrentProfile.Name+"\", \"qstatus\":\""+a.QueueStatus+"\"}");
                    //Thread.Sleep(1500);
                    //SendData("Extension " + a.Number + " => " + a.FirstName + " => " + a.LastName + " => " + a.CurrentProfile.Name + " => " + a.QueueStatus);

                }

            }

            //foreach (DN dn in PhoneSystem.Root.GetDN())
            //{
            //    System.Console.WriteLine(dn + " " + (dn.IsRegistered ? "REGISTERED" : "NOT REGISTERED") + "VMB(" + dn.VoiceMailBox.ToString() + ")");
            //    ActiveConnection[] a = dn.GetActiveConnections();
            //    ActiveConnection[] ab = dn.GetActiveConnections();
            //    Extension ext = dn as Extension;

            //    //if (ext != null)
            //    //{
            //    //    ext.CurrentProfile = ext.FwdProfiles[3];
            //    //    System.Console.WriteLine("JOEL - HERE IS THE DATA - " + ext.CurrentProfile);
            //    //    //ext.Save();
            //    //}
            //    if (dn.IsRegistered || a.Length > 0)
            //    {
            //        foreach (ActiveConnection ac in a)
            //        {
            //            System.Console.WriteLine(ac.CallID);
            //            System.Console.WriteLine(ac.InternalParty);
            //            System.Console.WriteLine(ac.IsInbound);
            //            System.Console.WriteLine(ac.IsOutbound);
            //            System.Console.WriteLine(ac.Status);
            //            System.Console.WriteLine(DateTime.Now - ac.LastChangeStatus);
            //            System.Console.WriteLine(ac.ExternalParty);
            //            System.Console.WriteLine("AttachedData(" + ac.AttachedData.Count.ToString() + "):");
            //            foreach (KeyValuePair<String, String> kvp in ac.AttachedData)
            //            {
            //                System.Console.WriteLine(kvp.Key + "=" + kvp.Value);
            //            }
            //        }
            //    }
            //}
            //foreach (Queue q in PhoneSystem.Root.GetQueues())
            //{
            //    foreach (QueueAgent g in q.QueueAgents)
            //    {
            //        System.Console.WriteLine("[QUEUE MEMBER] " + q.Name + "; DN: " + g.DN + "; QueueStatus: " + g.QueueStatus.ToString());
            //    }
            //}

            while (true)
            {
                Thread.Sleep(750);
            }
        }

    }
}
