﻿using System;
using System.Linq;
using System.Threading;
using TCX.Configuration;

namespace OMSamples
{
    class MakeCallSample
    {
        public void Run(params string[] args)
        {
            for (; ; )
            {
                var callid = args[0];
                var callid2 = args[1];
                try
                {
                    PhoneSystem.Root.MakeCall(args[0], args[1]);
                    Console.WriteLine("Calling :" + args[0] + " to " + args[1]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
          
            }
        }
    }
}
