﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Net.Sockets;
using TCX.PBXAPI;
using TCX.Configuration;
using Quobject.SocketIoClientDotNet.Client;
//using leSocket = Quobject.SocketIoClientDotNet.Client.Socket;


namespace OMSamples
{
    
    class MyListener
    {
        //static string HOST = "127.0.0.1";
        //static int PORT = 3113;

        //static TcpClient client;

        //static void OpenConnection()


        //{
        //    if (client != null)
        //    {
        //        Console.ForegroundColor = ConsoleColor.Red;
        //        Console.WriteLine("--Connection is already open --");
        //    }
        //    else
        //    {
        //        try
        //        {
        //            client = new TcpClient();
        //            client.Connect(HOST, PORT);
        //            Console.ForegroundColor = ConsoleColor.Green;
        //            Console.WriteLine(" The Connection Succeeded Because Joel is Awesome");
        //        }
        //        catch (Exception ex)
        //        {
        //            client = null;
        //            Console.ForegroundColor = ConsoleColor.Red;
        //            Console.WriteLine("ERROR - Connection could not be established because things are BAD: " + ex.Message);
        //        }
        //    }
        //}

        //static void SendData(string data)
        //{
        //    if (client == null)
        //    {
        //        Console.ForegroundColor = ConsoleColor.Red;
        //        Console.WriteLine("--Connection is not yet open --");
        //    }

        //    NetworkStream nwStream = client.GetStream();
        //    byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(data);
        //    Console.ForegroundColor = ConsoleColor.Cyan;
        //    Console.WriteLine("Sending: " + data);
        //    nwStream.Write(bytesToSend, 0, bytesToSend.Length);

        //}
        //var lesocket = IO.Socket("http://localhost:3035");
        static int receivedNotifications = 0;
        public class MyEvent
        {
            
            public MyEvent(int operation, NotificationEventArgs e)
            {
                operation_ = operation;
                e_ = e;
            }
            public int operation_;
            public NotificationEventArgs e_;
        };

        String filter_ = null;
        public MyListener(String filter)
        {
            filter_ = filter;
        }

        void eventHandler(Object data)
        {
            //OpenConnection();

            MyEvent ev = data as MyEvent;
            if (ev != null)
            {
                //Connect to Socket.IO?
                var lesocket = IO.Socket("http://10.254.1.7:3035");
                lesocket.On(Socket.EVENT_CONNECT, () =>
                {
                    lesocket.Emit("3CXConn");
                });
                NotificationEventArgs e = ev.e_;
                switch (ev.operation_)
                {
                    case 0: //insert
                        {
                            System.Console.WriteLine("Inserted: " + e.EntityName + "(" + e.RecID.ToString() + "):" + (e.ConfObject == null ? "no object" : e.ConfObject.ToString()));
                            ActiveConnection ac = e.ConfObject as ActiveConnection;
                            if (ac != null)
                            {
                                System.Console.WriteLine("AttachedData(" + ac.AttachedData.Count.ToString() + "):");
                                foreach (KeyValuePair<String, String> kvp in ac.AttachedData)
                                {
                                    System.Console.WriteLine(kvp.Key + "=" + kvp.Value);
                                }
                            }
                            if (e.EntityName == "REGISTRATION")
                            {
                                DN dn = e.ConfObject as DN;
                                foreach (RegistrarRecord rr in dn.GetRegistrarContactsEx())
                                {
                                    System.Console.WriteLine(rr.ToString() + "\nExpites=" + rr.Expires.ToString());
                                }
                            }
                        }
                        break;
                    case 1:
                        {
                            System.Console.WriteLine("Updated: " + e.EntityName + "(" + e.RecID.ToString() + "):" + (e.ConfObject == null ? "no object" : e.ConfObject.ToString()));
                            System.Console.WriteLine("JOEL - CHECK HERE - " + e.RecID.ToString());
                            var extnshn = e.RecID.ToString();


                            // Joels Janky Code


                            // Below Deals with incomming calls to Queue

                            foreach (Queue q in PhoneSystem.Root.GetQueues())
                            {
                                var inQ = q.GetActiveConnections();
                                System.Console.WriteLine("Queue " + q.Name + "; Calls in Q = " + inQ.Length);
                                //SendData("{\"queueName\":\"" + q.Name + "\", \"callsInQueue\":\"" + inQ.Length + "\"}");
                                //Thread.Sleep(100);
                                var msg = "{\"queueName\":\"" + q.Name + "\", \"callsInQueue\":\"" + inQ.Length + "\"}";
                                    lesocket.Emit("Qs", msg);
                                foreach (ActiveConnection a1 in q.GetActiveConnections())
                                {
                                    System.Console.WriteLine("Active: " + a1.ExternalParty);
                                }
                            }

                            // Below deals with Extension status / details
                            //DN dn1 = PhoneSystem.Root.GetDNByNumber(e.RecID.ToString());
                            //foreach (Extension a in PhoneSystem.Root.GetTenants()[0].GetExtensions())
                            //{
                            //    if (a.IsRegistered)
                            //    {
                            //        DN thisdn = PhoneSystem.Root.GetDNByNumber(a.Number);
                            //        //System.Console.WriteLine("Groups = " + a.Groups[2]);
                            //        foreach(var gr in a.Groups){
                            //            var pattern = "__DEFAULT__";
                            //            var input = gr.ToString();
                            //            if (pattern.Equals(input))
                            //            //if (gr.ToString() == defGrName)
                            //            {
                            //                System.Console.WriteLine("Belongs to Default Group");
                            //                //SendData("Belongs to " + defGrName +"\n");
                            //            }else{
                            //                System.Console.WriteLine("Belongs to: " + gr.ToString());
                            //                //SendData("Belongs to: " + gr.ToString() + "\n");
                            //                var grps = gr.ToString(); 
                            //                lesocket.Emit("groups", grps);

                                            
                            //            }
                                          
                            //        }


                            //        foreach(var pr in a.GetActiveConnections())
                            //        {
                            //            System.Console.WriteLine("active - " + pr.LastChangeStatus);
                            //            //SendData("active - " + pr.Status + "\n");
                            //            var liveCall = "{\"extension\":\"" + a.Number + "\", \"status\":\"" + pr.Status + "\"}";
                            //            lesocket.Emit("Call", liveCall);
                            //            //Thread.Sleep(1000);
                            //            System.Console.WriteLine("active - " + pr.Status);
                            //        }

                            //        System.Console.WriteLine("Extension " + a.Number + " " + a.FirstName + " " + a.LastName + " => " + a.CurrentProfile.Name);
                            //        var extMsg = "{\"extension\":\"" + a.Number + "\", \"firstname\":\"" + a.FirstName + "\", \"lastname\":\"" + a.LastName + "\", \"status\":\"" + a.CurrentProfile.Name + "\", \"qstatus\":\"" + a.QueueStatus + "\"}";
                            //        lesocket.Emit("extUpd", extMsg);
                            //        //SendData("{\"extension\":\""+a.Number+"\", \"firstname\":\""+a.FirstName+"\", \"lastname\":\""+a.LastName+"\", \"status\":\""+a.CurrentProfile.Name+"\", \"qstatus\":\""+a.QueueStatus+"\"}");
                            //        //Thread.Sleep(1500);
                            //        //SendData("Extension " + a.Number + " => " + a.FirstName + " => " + a.LastName + " => " + a.CurrentProfile.Name + " => " + a.QueueStatus);

                            //    }

                            //}


                            //// Just some breaks

                            //foreach (Queue q in PhoneSystem.Root.GetQueues())
                            //{
                            //    foreach (QueueAgent g in q.QueueAgents)
                            //    {
                            //        System.Console.WriteLine("[QUEUE MEMBER] " + q.Name + "; DN: " + g.DN + "; QueueStatus: " + g.QueueStatus.ToString());
                            //    }
                            //}
                            // End the Jank - My balls are broken. 
                            //ActiveConnection ac = e.ConfObject as ActiveConnection;
                            //if (ac != null)
                            //{
                            //    System.Console.WriteLine("AttachedData(" + ac.AttachedData.Count.ToString() + "):");
                            //    foreach (KeyValuePair<String, String> kvp in ac.AttachedData)
                            //    {
                            //        System.Console.WriteLine(kvp.Key + "=" + kvp.Value);
                            //    }
                            //}
                            //if (e.EntityName == "REGISTRATION")
                            //{
                            //    DN dn = e.ConfObject as DN;
                            //    //System.Console.WriteLine("JOEL --> " + dn.GetProperties);
                            //    foreach (RegistrarRecord rr in dn.GetRegistrarContactsEx())
                            //    {
                            //        System.Console.WriteLine(rr.ToString() + "\nExpites=" + rr.Expires.ToString());

                            //    }
                            //}
                        }
                        break;
                    case 2:
                        {
                            //System.Console.WriteLine("Deleted: " + e.EntityName + "(" + e.RecID.ToString() + "):" + (e.ConfObject == null ? "no object" : e.ConfObject.ToString()));

                            //ActiveConnection ac = e.ConfObject as ActiveConnection;
                            //if (ac != null)
                            //{
                            //    System.Console.WriteLine("AttachedData(" + ac.AttachedData.Count.ToString() + "):");
                            //    foreach (KeyValuePair<String, String> kvp in ac.AttachedData)
                            //    {
                            //        System.Console.WriteLine(kvp.Key + "=" + kvp.Value);
                            //    }
                            //}
                            //if (e.EntityName == "REGISTRATION")
                            //{
                            //    DN dn = e.ConfObject as DN;
                            //    foreach (RegistrarRecord rr in dn.GetRegistrarContactsEx())
                            //    {
                            //        System.Console.WriteLine(rr.ToString() + "\nExpites=" + rr.Expires.ToString());
                            //    }
                            //}
                        }
                        break;
                    default:
                        break;
                }

            }
        }

        public void ps_Inserted(object sender, NotificationEventArgs e)
        {
            if (filter_ != null && filter_ != e.EntityName)
                return;
            //System.Console.WriteLine((receivedNotifications++).ToString());
            //ThreadPool.QueueUserWorkItem(this.eventHandler, new MyEvent(0, e));
            this.eventHandler(new MyEvent(0, e));
        }
        public void ps_Updated(object sender, NotificationEventArgs e)
        {
            if (filter_ != null && filter_ != e.EntityName)
                return;
            //System.Console.WriteLine((receivedNotifications++).ToString());
            //ThreadPool.QueueUserWorkItem(this.eventHandler, new MyEvent(1, e));
            this.eventHandler(new MyEvent(1, e));
        }
        public void ps_Deleted(object sender, NotificationEventArgs e)
        {
            if (filter_ != null && filter_ != e.EntityName)
                return;
            //System.Console.WriteLine((receivedNotifications++).ToString());
            //ThreadPool.QueueUserWorkItem(this.eventHandler, new MyEvent(2, e));
            this.eventHandler(new MyEvent(2, e));
        }
    }
}
